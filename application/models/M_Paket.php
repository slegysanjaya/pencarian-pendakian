<?php
class M_Paket extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		return $this->db->get('tb_paket');
	} 


	public function get_by_id($id) {
		return $this->db->get_where('tb_paket', array('id' => $id));
	}

	public function get_detail_by_id($id) {
		return $this->db->get_where('detail_paket', array('id' => $id));
	}

	public function get_all_detail() {
		return $this->db->get('detail_paket');
	} 

	public function tambah($data) {
		$this->db->insert('tb_paket', $data);
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Tambah data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Tambah data tidak berhasil!</div>');
	}

	public function tambah_detail($data) {
		$this->db->insert('detail_paket', $data);
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Tambah data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Tambah data tidak berhasil!</div>');
	}

	public function edit($data, $id) {
		$this->db->update('tb_paket', $data, array('id' => $id));
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Edit data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Edit data tidak berhasil!</div>');
	}

	public function hapus($id) {
		$this->db->delete('tb_paket', array('id'=>$id));

		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Hapus data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Hapus data tidak berhasil!</div>');
	}

}