<?php
class M_Pendakian extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		return $this->db->get('tb_pendakian');
	} 

	public function get_by_id($id) {
		return $this->db->get_where('tb_pendakian', array('id' => $id));
	}

	public function get_pendakian_provinsi($provinsi){
		$this->db->select('a.*, b.nama_paket, b.harga');
		$this->db->from('tb_pendakian a');
		$this->db->join('tb_paket b', 'a.id_paket = b.id','left');
		$this->db->where('a.provinsi', $provinsi);
		return $this->db->get();
	}

	public function get_pendakian(){
		$this->db->select('a.*, b.nama_paket, b.harga');
		$this->db->from('tb_pendakian a');
		$this->db->join('tb_paket b', 'a.id_paket = b.id','left');
		return $this->db->get();
	}

	public function tambah($data) {
		$this->db->insert('tb_pendakian', $data);
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Tambah data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Tambah data tidak berhasil!</div>');
	}

	public function edit($data, $id) {
		$this->db->update('tb_pendakian', $data, array('id' => $id));
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Edit data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Edit data tidak berhasil!</div>');
	}

	public function hapus($id) {
		$this->db->delete('tb_pendakian', array('id'=>$id));

		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Hapus data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Hapus data tidak berhasil!</div>');
	}

}