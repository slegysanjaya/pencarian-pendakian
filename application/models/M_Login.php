<?php
class M_Login extends CI_Model {

	function cek_login($data) {
		$user = $this->db->get_where('tb_user', $data);

		if($user->num_rows() == 1) {
			$data_session['id_user'] = $user->row()->id_user;
			$data_session['username'] = $user->row()->username;
			$data_session['level'] = $user->row()->level;
			$this->session->set_userdata($data_session);
			return true;
		} else {
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger"><strong>Login gagal</strong><br>Periksa Username dan Password Anda!</div>');
			return false;
		}
	}

	public function tambah($data) {
		$this->db->insert('tb_user', $data);
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Tambah data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Tambah data tidak berhasil!</div>');
	}

}