<?php
class M_info_gunung extends CI_Model {
	
	public function __construct() {
		parent::__construct();
	}

	public function get_all() {
		return $this->db->get('tb_info_gunung');
	} 


	public function get_by_id($id) {
		return $this->db->get_where('tb_info_gunung', array('id' => $id));
	}

	public function get_detail($id) {
		return $this->db->get_where('tb_info_gunung', array('id' => $id));
	}

	public function get_adetail() {
		return $this->db->get('detail_paket');
	} 

	public function tambah($data) {
		$this->db->insert('tb_info_gunung', $data);
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Tambah data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Tambah data tidak berhasil!</div>');
	}

	public function edit($data, $id) {
		$this->db->update('tb_info_gunung', $data, array('id' => $id));
		
		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Edit data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Edit data tidak berhasil!</div>');
	}

	public function hapus($id) {
		$this->db->delete('tb_info_gunung', array('id'=>$id));

		if($this->db->affected_rows() > 0)
			$this->session->set_flashdata('pesan', '<div class="alert alert-success">Hapus data berhasil!</div>');
		else
			$this->session->set_flashdata('pesan', '<div class="alert alert-danger">Hapus data tidak berhasil!</div>');
	}

}