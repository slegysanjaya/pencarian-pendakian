<?php 
class Rekomendasi extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('M_Pendakian');
		$this->load->model('M_Paket');
		
	}
	
	public function index() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$this->load->view('rekomendasi/V_rekomendasi');
	}

	public function rekomen_user() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$this->load->view('rekomendasi/V_rekomendasi2');
	}

	public function proses(){
		//if($this->input->post('provinsi') == 'all'){
			$arr_tmp = $this->M_Pendakian->get_pendakian()->result_array();
		// }else{
		// 	$arr_tmp = $this->M_Pendakian->get_pendakian_provinsi($this->input->post('provinsi'))->result_array();
		// }

			// print_r($arr_tmp);
			// die();

		for($i=0; $i<count($arr_tmp); $i++){

			$from = urlencode('unjani, cimahi');
			$to = urlencode($arr_tmp[$i]['wilayah']);

			$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=id-ID&sensor=false");
			$data = json_decode($data);

			$distance = 0;

			foreach($data->rows[0]->elements as $road) {
			    $distance += $road->distance->value;
			}
			$arr[$i]['id'] = $arr_tmp[$i]['id'];
			$arr[$i]['alternatif'] = $arr_tmp[$i]['nama_wisata'];
			$arr[$i]['wilayah'] = $arr_tmp[$i]['wilayah'];
			$arr[$i]['ketinggian'] = $arr_tmp[$i]['ketinggian'];
			$arr[$i]['transportasi'] = 'Bus';
			$arr[$i]['biaya_masuk'] = $arr_tmp[$i]['biaya_masuk'];
			// $arr[$i]['id_paket'] = $arr_tmp[$i]['id_paket'];
			// $arr[$i]['nama_paket'] = $arr_tmp[$i]['nama_paket'];
			$arr[$i]['harga'] = $arr_tmp[$i]['id_paket'];
			$arr[$i]['jarak'] = ($distance / 1000)*2;
			$arr[$i]['provinsi'] = $arr_tmp[$i]['provinsi'];
		}

		for($i=0; $i<count($arr); $i++){
			$arr_bobot[$i]['alternatif'] = $arr[$i]['alternatif'];
			$arr_bobot[$i]['jarak'] = $arr[$i]['jarak'];


			if($arr[$i]['jarak'] > 1800){
				$jml_makan = 8*3;
			}elseif ($arr[$i]['jarak'] >= 1500 && $arr[$i]['jarak'] <= 1799) {
				$jml_makan = 7*3;
			}elseif ($arr[$i]['jarak'] >= 1200 && $arr[$i]['jarak'] <= 1499) {
				$jml_makan = 6*3;
			}elseif ($arr[$i]['jarak'] >= 900 && $arr[$i]['jarak'] <= 1199) {
				$jml_makan = 5*3;
			}elseif ($arr[$i]['jarak'] >= 600 && $arr[$i]['jarak'] <= 899) {
				$jml_makan = 4*3;
			}elseif ($arr[$i]['jarak'] >= 300 && $arr[$i]['jarak'] <= 599) {
				$jml_makan = 3*3;
			}elseif ($arr[$i]['jarak'] >= 100 && $arr[$i]['jarak'] <= 299) {
				$jml_makan = 2*3;
			}elseif ($arr[$i]['jarak'] < 99) {
				$jml_makan = 1*3;
			}

			// if($arr[$i]['jarak'] > 501){
			// 	$arr_bobot[$i]['jarak'] = 1;
			// 	$jml_makan = 4*3;
			// }elseif ($arr[$i]['jarak'] >= 251 && $arr[$i]['jarak'] <= 500) {
			// 	$jml_makan = 3*3;
			// 	$arr_bobot[$i]['jarak'] = 2;
			// }elseif ($arr[$i]['jarak'] >= 101 && $arr[$i]['jarak'] <= 250) {
			// 	$jml_makan = 2*3;
			// 	$arr_bobot[$i]['jarak'] = 3;
			// }elseif ($arr[$i]['jarak'] < 100) {
			// 	$jml_makan = 1*3;
			// 	$arr_bobot[$i]['jarak'] = 4;
			// }

			$arr_bobot[$i]['ketinggian'] = $arr[$i]['ketinggian'];
			// if($arr[$i]['ketinggian'] > 3001){
			// 	$arr_bobot[$i]['ketinggian'] = 1;
			// }elseif ($arr[$i]['ketinggian'] >= 2001 && $arr[$i]['ketinggian'] <= 3000) {
			// 	$arr_bobot[$i]['ketinggian'] = 2;
			// }elseif ($arr[$i]['ketinggian'] >= 1001 && $arr[$i]['ketinggian'] <= 2000) {
			// 	$arr_bobot[$i]['ketinggian'] = 3;
			// }elseif ($arr[$i]['ketinggian'] < 1000) {
			// 	$arr_bobot[$i]['ketinggian'] = 4;
			// }

			if($arr[$i]['transportasi'] == 'Kendaraan Pribadi'){
				$arr_bobot[$i]['transportasi'] = 3;
				$satuan = 1000;
			}elseif ($arr[$i]['transportasi'] == 'Bus' ) {
				$arr_bobot[$i]['transportasi'] = 1;
				$satuan = 177;
			}elseif ($arr[$i]['transportasi'] == 'Kereta Api') {
				$arr_bobot[$i]['transportasi'] = 2;
				$satuan = 200;
			}

			// $biaya[$i] = (($arr[$i]['jarak']*$satuan)*2)+($arr[$i]['biaya_masuk']+($arr[$i]['harga']*$jml_makan));
			$biaya[$i] = round(((($arr[$i]['jarak']*177)*2)+($arr[$i]['biaya_masuk']+($arr[$i]['harga']*$jml_makan)))/1000, 2);
			$arr[$i]['total_biaya'] = $biaya[$i];

			$arr_bobot[$i]['biaya'] = $arr[$i]['total_biaya'];
			$arr[$i]['bobot_biaya'] = $arr[$i]['total_biaya'];
			// if($biaya[$i] < 150000 ){
			// 	$arr_bobot[$i]['biaya'] = 4;
			// 	$arr[$i]['bobot_biaya'] = 4;
			// }
			// if ($biaya[$i] >= 150001 && $biaya[$i] <= 300000 ) {
			// 	$arr_bobot[$i]['biaya'] = 3;
			// 	$arr[$i]['bobot_biaya'] = 3;
			// }
			// if ($biaya[$i] >= 300001 && $biaya[$i] <= 450000 ) {
			// 	$arr_bobot[$i]['biaya'] = 2;
			// 	$arr[$i]['bobot_biaya'] = 2;
			// }
			// if ($biaya[$i] > 450001) {
			// 	$arr_bobot[$i]['biaya'] = 1;
			// 	$arr[$i]['bobot_biaya'] = 1;
			// }



			//$arr_bobot[$i]['id_paket'] = $arr[$i]['id_paket'];
			$arr_bobot[$i]['id'] = $arr[$i]['id'];

		}

		// print_r($arr_bobot);
		// die();


		// print_r($biaya);
		// die();

		$kirim['biaya_user'] = $this->input->post('biaya');
		// print_r($arr_bobot);
		// die();
		$kirim['arr'] = $arr;
		$kirim['data_bobot'] = $arr_bobot;


		$this->load->view('rekomendasi/V_proses', $kirim);
		
	}

	public function proses2(){
		//if($this->input->post('provinsi') == 'all'){
			$arr_tmp = $this->M_Pendakian->get_pendakian()->result_array();
		// }else{
		// 	$arr_tmp = $this->M_Pendakian->get_pendakian_provinsi($this->input->post('provinsi'))->result_array();
		// }

			// print_r($arr_tmp);
			// die();

		for($i=0; $i<count($arr_tmp); $i++){

			$from = urlencode('unjani, cimahi');
			$to = urlencode($arr_tmp[$i]['wilayah']);

			$data = file_get_contents("http://maps.googleapis.com/maps/api/distancematrix/json?origins=$from&destinations=$to&language=id-ID&sensor=false");
			$data = json_decode($data);

			$distance = 0;

			foreach($data->rows[0]->elements as $road) {
			    $distance += $road->distance->value;
			}
			$arr[$i]['id'] = $arr_tmp[$i]['id'];
			$arr[$i]['alternatif'] = $arr_tmp[$i]['nama_wisata'];
			$arr[$i]['wilayah'] = $arr_tmp[$i]['wilayah'];
			$arr[$i]['ketinggian'] = $arr_tmp[$i]['ketinggian'];
			$arr[$i]['transportasi'] = 'Bus';
			$arr[$i]['biaya_masuk'] = $arr_tmp[$i]['biaya_masuk'];
			// $arr[$i]['id_paket'] = $arr_tmp[$i]['id_paket'];
			// $arr[$i]['nama_paket'] = $arr_tmp[$i]['nama_paket'];
			$arr[$i]['harga'] = $arr_tmp[$i]['id_paket'];
			$arr[$i]['jarak'] = ($distance / 1000)*2;
			$arr[$i]['provinsi'] = $arr_tmp[$i]['provinsi'];
		}

		for($i=0; $i<count($arr); $i++){
			$arr_bobot[$i]['alternatif'] = $arr[$i]['alternatif'];
			$arr_bobot[$i]['jarak'] = $arr[$i]['jarak'];


			if($arr[$i]['jarak'] > 1800){
				$jml_makan = 8*3;
			}elseif ($arr[$i]['jarak'] >= 1500 && $arr[$i]['jarak'] <= 1799) {
				$jml_makan = 7*3;
			}elseif ($arr[$i]['jarak'] >= 1200 && $arr[$i]['jarak'] <= 1499) {
				$jml_makan = 6*3;
			}elseif ($arr[$i]['jarak'] >= 900 && $arr[$i]['jarak'] <= 1199) {
				$jml_makan = 5*3;
			}elseif ($arr[$i]['jarak'] >= 600 && $arr[$i]['jarak'] <= 899) {
				$jml_makan = 4*3;
			}elseif ($arr[$i]['jarak'] >= 300 && $arr[$i]['jarak'] <= 599) {
				$jml_makan = 3*3;
			}elseif ($arr[$i]['jarak'] >= 100 && $arr[$i]['jarak'] <= 299) {
				$jml_makan = 2*3;
			}elseif ($arr[$i]['jarak'] < 99) {
				$jml_makan = 1*3;
			}

			// if($arr[$i]['jarak'] > 501){
			// 	$arr_bobot[$i]['jarak'] = 1;
			// 	$jml_makan = 4*3;
			// }elseif ($arr[$i]['jarak'] >= 251 && $arr[$i]['jarak'] <= 500) {
			// 	$jml_makan = 3*3;
			// 	$arr_bobot[$i]['jarak'] = 2;
			// }elseif ($arr[$i]['jarak'] >= 101 && $arr[$i]['jarak'] <= 250) {
			// 	$jml_makan = 2*3;
			// 	$arr_bobot[$i]['jarak'] = 3;
			// }elseif ($arr[$i]['jarak'] < 100) {
			// 	$jml_makan = 1*3;
			// 	$arr_bobot[$i]['jarak'] = 4;
			// }

			$arr_bobot[$i]['ketinggian'] = $arr[$i]['ketinggian'];
			// if($arr[$i]['ketinggian'] > 3001){
			// 	$arr_bobot[$i]['ketinggian'] = 1;
			// }elseif ($arr[$i]['ketinggian'] >= 2001 && $arr[$i]['ketinggian'] <= 3000) {
			// 	$arr_bobot[$i]['ketinggian'] = 2;
			// }elseif ($arr[$i]['ketinggian'] >= 1001 && $arr[$i]['ketinggian'] <= 2000) {
			// 	$arr_bobot[$i]['ketinggian'] = 3;
			// }elseif ($arr[$i]['ketinggian'] < 1000) {
			// 	$arr_bobot[$i]['ketinggian'] = 4;
			// }

			if($arr[$i]['transportasi'] == 'Kendaraan Pribadi'){
				$arr_bobot[$i]['transportasi'] = 3;
				$satuan = 1000;
			}elseif ($arr[$i]['transportasi'] == 'Bus' ) {
				$arr_bobot[$i]['transportasi'] = 1;
				$satuan = 177;
			}elseif ($arr[$i]['transportasi'] == 'Kereta Api') {
				$arr_bobot[$i]['transportasi'] = 2;
				$satuan = 200;
			}

			// $biaya[$i] = (($arr[$i]['jarak']*$satuan)*2)+($arr[$i]['biaya_masuk']+($arr[$i]['harga']*$jml_makan));
			$biaya[$i] = round(((($arr[$i]['jarak']*177)*2)+($arr[$i]['biaya_masuk']+($arr[$i]['harga']*$jml_makan)))/1000, 2);
			$arr[$i]['total_biaya'] = $biaya[$i];

			$arr_bobot[$i]['biaya'] = $arr[$i]['total_biaya'];
			$arr[$i]['bobot_biaya'] = $arr[$i]['total_biaya'];
			// if($biaya[$i] < 150000 ){
			// 	$arr_bobot[$i]['biaya'] = 4;
			// 	$arr[$i]['bobot_biaya'] = 4;
			// }
			// if ($biaya[$i] >= 150001 && $biaya[$i] <= 300000 ) {
			// 	$arr_bobot[$i]['biaya'] = 3;
			// 	$arr[$i]['bobot_biaya'] = 3;
			// }
			// if ($biaya[$i] >= 300001 && $biaya[$i] <= 450000 ) {
			// 	$arr_bobot[$i]['biaya'] = 2;
			// 	$arr[$i]['bobot_biaya'] = 2;
			// }
			// if ($biaya[$i] > 450001) {
			// 	$arr_bobot[$i]['biaya'] = 1;
			// 	$arr[$i]['bobot_biaya'] = 1;
			// }



			//$arr_bobot[$i]['id_paket'] = $arr[$i]['id_paket'];
			$arr_bobot[$i]['id'] = $arr[$i]['id'];

		}

		// print_r($arr_bobot);
		// die();


		// print_r($biaya);
		// die();

		$kirim['biaya_user'] = $this->input->post('biaya');
		// print_r($arr_bobot);
		// die();
		$kirim['arr'] = $arr;
		$kirim['data_bobot'] = $arr_bobot;


		$this->load->view('rekomendasi/V_proses2', $kirim);
		
	}
	
}