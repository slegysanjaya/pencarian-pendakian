<?php
class Login extends CI_Controller {
	
	public function __construct() {
		parent::__construct();

		$this->load->model('M_Login');
	}
	
	public function index() {
		if($this->session->userdata('level') == null) {
			$this->load->view('login/V_Login');
		} else {
			redirect(base_url());
		}
	}

	public function daftar() {
   		$url = base_url().'Login/daftar';
		if(isset($_POST['simpan'])) {  

			$cek_username=mysql_num_rows(mysql_query
             ("SELECT username FROM tb_user 
               WHERE username='$_POST[username]'"));
			// Kalau username sudah ada yang pakai
			if ($cek_username > 0){
				echo "<script language=\"Javascript\">\n";
				echo "confirmed = window.confirm('Username sudah dipakai. Coba yang lain!!');";
				echo "if (confirmed)";
				echo "{";
				echo "window.location = '".$url."'";
				echo "}";
				echo "else ";
				echo "{";
				echo "window.location = '".$url."'";
				echo "}";
				echo "</script>";

			}
			// Kalau username valid, inputkan data ke tabel users
			else{

				$data = array(
					'username' => $this->input->post('username'),
					'password' => $this->input->post('password'),
					'level' => 'pengguna'
				);
				$this->M_Login->tambah($data);

				redirect(base_url().'login','refresh');
			}
		} else {
			
			$this->load->view('login/v_daftar');
		}
	}

	public function cek_login() {
		$data = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password')
		);
		
		$hasil = $this->M_Login->cek_login($data);

		if($hasil)
			redirect(base_url());
		else
			redirect(base_url().'login');
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url(),'refresh');
	}
	
}