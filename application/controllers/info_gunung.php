<?php 
class info_gunung extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('M_info_gunung');
		$this->load->helper(array('form', 'url'));
		
	}
	
	public function index() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$data['data_gunung'] = $this->M_info_gunung->get_all();
		$this->load->view('info_gunung/V_info_gunung', $data);
	}

	public function detail_info_gunung() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$data['data_detail'] = $this->M_info_gunung->get_detail($this->uri->segment(3))->row();
		$this->load->view('info_gunung/V_detail_info_gunung', $data);
	}

	public function edit() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		$data = array(
			'nama_gunung' => $this->input->post('nama_gunung'),
			'keterangan' => $this->input->post('keterangan')
		);
		
		$this->M_info_gunung->edit($data, $this->input->post('id'));
		redirect(base_url().'info_gunung', 'refresh');
		
	}
	public function showTambah() {
		$this->load->view('info_gunung/V_tambah_info');
	}

	public function tambah() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}


		$config['upload_path']          = './foto/';
        $config['allowed_types']        = 'gif|jpg|png';
        $config['max_size']             = 100000;
      

        $this->load->library('upload', $config);

        if ( ! $this->upload->do_upload('userfile'))
        {
                $error = array('error' => $this->upload->display_errors());

                $this->load->view('info_gunung/V_info_gunung', $pesan);
        }
        else
        {
                $foto =  $this->upload->data();

               $data = array(
					'nama_gunung' => $this->input->post('nama_gunung'),
					'keterangan' => $this->input->post('keterangan'),
					'foto' => $foto['file_name']
				);
				
				$this->M_info_gunung->tambah($data);
        }
		redirect(base_url().'info_gunung', 'refresh');
	}

	public function hapus() {

		$this->M_info_gunung->hapus($this->input->post('id'));
		redirect(base_url().'info_gunung','refresh');
	}


}