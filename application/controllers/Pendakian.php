<?php 
class Pendakian extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('M_Pendakian');
		$this->load->model('M_Paket');
		
	}
	
	public function index() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$data['data_pendakian'] = $this->M_Pendakian->get_pendakian();
		$data['paket'] = $this->M_Paket->get_all();
		$this->load->view('pendakian/V_data_Pendakian', $data);
	}
	
	public function edit() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		$data = array(
				'nama_wisata' => $this->input->post('nama_wisata'),
				'ketinggian' => $this->input->post('ketinggian'),
				'wilayah' => $this->input->post('wilayah'),
				'biaya_masuk' => $this->input->post('biaya_masuk'),
				'provinsi' => $this->input->post('provinsi'),
				'id_paket' => 45000
		);
		
		$this->M_Pendakian->edit($data, $this->input->post('id'));
		redirect(base_url().'pendakian', 'refresh');
		
	}

	public function tambah() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		if(isset($_POST['simpan'])) {
			$data = array(
				'nama_wisata' => $this->input->post('nama_wisata'),
				'ketinggian' => $this->input->post('ketinggian'),
				'wilayah' => $this->input->post('wilayah'),
				'biaya_masuk' => $this->input->post('biaya_masuk'),
				'provinsi' => $this->input->post('provinsi'),
				'id_paket' => 45000
			);
			$this->M_Pendakian->tambah($data);
			redirect(base_url().'pendakian','refresh');
		} else {
			$data['paket'] = $this->M_Paket->get_all();
			$this->load->view('pendakian/V_tambah_pendakian', $data);
		}
	}

	public function hapus() {

		$this->M_Pendakian->hapus($this->input->post('id'));
		redirect(base_url().'pendakian','refresh');
	}
}