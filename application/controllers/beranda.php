<?php
class beranda extends CI_Controller {
	
	public function __construct() {
		parent::__construct();		
	}
	
	public function index() {
		if ($this->session->userdata('level') == null){
			$this->load->view('login/V_Login');
		}else{
			$this->load->view('v_beranda');
		}
	}
   	
} 