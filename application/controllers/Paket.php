<?php 
class Paket extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('M_Paket');
		
	}
	
	public function index() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$data['data_paket'] = $this->M_Paket->get_all();
		$this->load->view('paket/V_paket', $data);
	}


	public function detail_paket() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}
		
		$data['data_detail'] = $this->M_Paket->get_all_detail($this->uri->segment(3));
		$this->load->view('paket/V_detail_paket', $data);
	}

	public function tambah_detail() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		if(isset($_POST['simpan'])) {
			$total = $this->input->post('kata');
			for($i=0; $i<count($total); $i++)
			{
				$data = array(
						'id_paket' => $this->input->post('id_paket'),
						'keterangan' => $total[$i]
					);
				$this->M_Paket->tambah_detail($data);
			}
			redirect(base_url().'Paket/detail_paket/'.$this->uri->segment(3),'refresh');
		} else {
			
			$this->load->view('Paket/V_tambah_detail');
		}
	}
	
	public function edit() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		$data = array(
			'nama_paket' => $this->input->post('nama_paket'),
			'harga' => $this->input->post('harga')
		);
		
		$this->M_Paket->edit($data, $this->input->post('id'));
		redirect(base_url().'Paket', 'refresh');
		
	}

	public function tambah() {
		if ($this->session->userdata('level')== null){
			redirect(base_url().'login', 'refresh');
		}

		if(isset($_POST['simpan'])) {
			$data = array(
				'nama_paket' => $this->input->post('nama_paket'),
				'harga' => $this->input->post('harga')
			);
			$this->M_Paket->tambah($data);
			redirect(base_url().'Paket','refresh');
		} else {
			
			$this->load->view('paket/V_tambah_paket');
		}
	}

	public function hapus() {

		$this->M_Paket->hapus($this->input->post('id'));
		redirect(base_url().'Paket','refresh');
	}


	public function hapus_detail() {

		$this->M_Paket->hapus($this->input->post('id'));
		redirect(base_url().'Paket/detail_paket/'.$this->uri->segment(3),'refresh');
	}
}