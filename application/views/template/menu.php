
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="<?php echo base_url()?>Login" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>Pendakian</b></span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
             
              <!-- User Account: style can be found in dropdown.less -->
              <?php if($this->session->userdata('level') != null) :  ?>
                <li class=" user user-menu">
                  <a href="<?php echo base_url(); ?>Login/logout">
                    <span class="hidden-xs">Logout</span>
                  </a>
                </li>
              <?php else : ?>
                <li class=" user user-menu">
                  <a href="<?php echo base_url(); ?>Login">
                    <span class="hidden-xs">Login</span>
                  </a>
                </li>
              <?php endif ?>
              <!-- Control Sidebar Toggle Button -->
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active treeview">
            <?php if($this->session->userdata('level')=="administrator"): ?>
              <li class="<?php if($this->uri->segment(1) == 'Pendakian') echo "active"; ?>">
                <a href="<?php echo base_url();?>Pendakian">
                  <i class="fa fa-th"></i> <span>Pendakian</span>
                </a>
              </li>
              <li class="<?php if($this->uri->segment(1) == 'info_gunung') echo "active"; ?>">
                <a href="<?php echo base_url();?>info_gunung">
                  <i class="fa fa-th"></i> <span>Informasi Gunung</span>
                </a>
              </li>
              <li class="<?php if($this->uri->segment(1) == 'Rekomendasi') echo "active"; ?>">
                <a href="<?php echo base_url();?>Rekomendasi">
                  <i class="fa fa-th"></i> <span>Hasil Analisa</span>
                </a>
              </li>
            <?php else : ?>
              <li class="<?php if($this->uri->segment(1) == 'Rekomendasi') echo "active"; ?>">
                <a href="<?php echo base_url();?>Rekomendasi/rekomen_user">
                  <i class="fa fa-th"></i> <span>Hasil Analisa</span>
                </a>
              </li>
               <li class="<?php if($this->uri->segment(1) == 'info_gunung') echo "active"; ?>">
                <a href="<?php echo base_url();?>info_gunung">
                  <i class="fa fa-th"></i> <span>Informasi Gunung</span>
                </a>
              </li>
            <?php endif ?>
            
        </section>
        <!-- /.sidebar -->
      </aside>
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->