<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/header_table'); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <?php $this->load->view('template/menu'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Detail Gunung
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Detail Gunung</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                  <center><h1><?php echo $data_detail->nama_gunung ?></h1></center>
                  <br>
                  <center><img style="width: 50%; height: 45%" src="<?php echo base_url(); ?>foto/<?php echo $data_detail->foto ?>"></center>
                  <p align="justify">
                    <?php echo $data_detail->keterangan; ?>
                  </p>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->



    <?php $this->load->view('template/bottom_table'); ?>
    

  </body>
</html>
