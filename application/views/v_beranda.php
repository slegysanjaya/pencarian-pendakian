<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/_header'); ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">

    <div id="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <?php $this->load->view('template/menu'); ?>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Dashboard
            <small></small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashbord</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <center><h1>SELAMAT DATANG <?php echo strtoupper($this->session->userdata('username')); ?></h1></center>
            <center><h3>Lokasi Anda Saat ini</h3></center>
            <br>
            <center>
                <p id="tampilkan"></p>
                <div id="mapcanvas"></div>
                </center>
                <script src="http://maps.google.com/maps/api/js"></script>
                 
                <script>
                var view = document.getElementById("tampilkan");
               
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition, showError);
                    } else {
                        view.innerHTML = "Yah browsernya ngga support Geolocation bro!";
                    }
              
                 
                function showPosition(position) {
                    lat = position.coords.latitude;
                    lon = position.coords.longitude;
                    latlon = new google.maps.LatLng(lat, lon)
                    mapcanvas = document.getElementById('mapcanvas')
                    mapcanvas.style.height = '480px';
                    mapcanvas.style.width = '1000px';
                 
                    var myOptions = {
                    center:latlon,
                    zoom:14,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                     
                    var map = new google.maps.Map(document.getElementById("mapcanvas"), myOptions);
                    var marker = new google.maps.Marker({
                        position:latlon,
                        map:map,
                        title:"You are here!"
                    });
                }
                 
                function showError(error) {
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            view.innerHTML = "Yah, mau deteksi lokasi tapi ga boleh :("
                            break;
                        case error.POSITION_UNAVAILABLE:
                            view.innerHTML = "Yah, Info lokasimu nggak bisa ditemukan nih"
                            break;
                        case error.TIMEOUT:
                            view.innerHTML = "Requestnya timeout bro"
                            break;
                        case error.UNKNOWN_ERROR:
                            view.innerHTML = "An unknown error occurred."
                            break;
                    }
                 }
                </script>

        </section>
      </aside><!-- /.control-sidebar -->
    </div>
    <!-- /#wrapper -->
</body>

    <?php $this->load->view('template/_bottom'); ?>
</html>