<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/header_table'); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <?php $this->load->view('template/menu'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Detail Gunung
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Detail Paket</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Keterangan</center>
                        <?php if($this->session->userdata('level') == 'administrator'): ?>
                          <th class="col-md-1"><center><a href="<?php echo base_url(); ?>Paket/tambah_detail/<?php echo $this->uri->segment(3); ?>" class="btn btn-primary btn-small" data-toggle="modal" ><i class="fa fa-plus"> Tambah Keterangan</i></a></center></th>
                        <?php endif ?>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_detail->result() as $key): ?>
                            <?php if($key->id_paket == $this->uri->segment(3)) :?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $key->keterangan; ?></center></td>
                            <?php if($this->session->userdata('level') == 'administrator'): ?>
                            <td ><center>
                                <div class="tooltip-demo">
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#modal_hapus_<?php echo $key->id; ?>" data-placement="top" title="Hapus" ><i class="fa fa-trash-o"> Hapus</i></a>
                                </div></center>
                            </td>
                          <?php endif ?>
                        </tr>
                            <?php endif ?>
                        <?php endforeach ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

<!-- Start Modal Hapus -->
<?php foreach ($data_detail->result() as $key): ?>
<div class="modal fade" id="modal_hapus_<?php echo $key->id; ?>">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body" style="margin-bottom:-20px;">
                <div class="alert alert-danger">Anda yakin akan menghapus data <strong><?php echo $key->keterangan; ?></strong> ?</div>
            </div>
            <div class="modal-footer">
                <form method="post" action="<?php echo base_url(); ?>Paket/hapus_detail/<?php echo $key->id; ?>">
                    <input name="id" type="hidden" value="<?php echo $key->id; ?>" >
                    <button type="submit" class="btn btn-default btn-primary" href="">Ya</button>
                    <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
<!-- End Start Modal Hapus -->


    <?php $this->load->view('template/bottom_table'); ?>
    

  </body>
</html>
