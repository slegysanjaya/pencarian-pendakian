<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/_header'); ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">


    <div id="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <?php $this->load->view('template/menu'); ?>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tambah Stopword</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Wordnet</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url(); ?>Paket/tambah_detail/<?php echo $this->uri->segment(3) ?>">
                  <div class="box-body" id="modal_tambah_table">

                    <div class="tooltip-demo">
                        <a id='AddRow' class="btn btn-success form-control"> Tambah Keterangan</a>
                    </div>

                    <div id="test" class="form-group">
                      <label for="exampleInputEmail1">Keterangan</label>
                      <input type="text" name="kata[]" class="form-control" id="exampleInputEmail1" placeholder="kata" >
                    </div>
                    <input type="text" name="id_paket" value="<?php echo $this->uri->segment(3); ?>" class="form-control hidden" id="exampleInputEmail1" placeholder="kata" >
                  </div><!-- /.box-body -->


                  <div class="box-footer">
                    <button type="submit" value="simpan" name="simpan" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
        </section>
      </aside><!-- /.control-sidebar -->
    </div>
    <!-- /#wrapper -->
</body>

    <?php $this->load->view('template/_bottom'); ?>

<script type="text/javascript">
var upload_number = 2;
$("#AddRow").click(function(e){
    e.preventDefault(); // biar gak load web
    var baris = $('div#test').clone(true); // duplikat tr dengan id=tmp
    baris.removeAttr('id'); // menghapus id, supaya tidak ganda
    baris.children('div').children('a').css('visibility', 'visible'); // get anak, seting css
    baris.appendTo('#modal_tambah_table'); // fungsi menambahkan
});

$("#delRow").click(function(){
    $(this).parents("tr").remove();
    return false;
});
</script>


</html>