<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/_header'); ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">


    <div id="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <?php $this->load->view('template/menu'); ?>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tambah Paket</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Paket</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url(); ?>Paket/tambah">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Paket</label>
                      <input type="text" name="nama_paket" class="form-control" id="exampleInputEmail1" placeholder="Nama Paket">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Harga</label>
                      <input type="text" name="harga" class="form-control" id="exampleInputEmail1" placeholder="harga">
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" value="simpan" name="simpan" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
        </section>
      </aside><!-- /.control-sidebar -->
    </div>
    <!-- /#wrapper -->
</body>

    <?php $this->load->view('template/_bottom'); ?>
</html>