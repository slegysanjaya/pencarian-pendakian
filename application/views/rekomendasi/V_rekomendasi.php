<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/_header'); ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">


    <div id="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <?php $this->load->view('template/menu'); ?>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hasil Analisa</li>
          </ol>
        </section>
        <br>

        <!-- Main content -->
        <section class="content">
            <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">SPK Wisata Pendakian</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url(); ?>Rekomendasi/proses">
                  <div class="box-body">
                    
    

                    <div class="form-group">
                      <label for="exampleInputEmail1">Biaya</label>
                      <input type="text" class="form-control" required name="biaya" >
                      <!-- <select name="biaya" class="form-control" id="exampleInputEmail1" required>
                            <option value="">Pilih Rentan Biaya</option>
                            <option value="4"> Kurang dari 150.000</option>
                            <option value="3">150.001 - 300.000</option>
                            <option value="2">300.001 - 450.000</option>
                            <option value="1"> Lebih dari 450.000</option>
                        
                      </select> --> 
                    </div>
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" value="simpan" name="simpan" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>


          
            <!-- left column -->
            <div class="col-md-5">
              <center>
                <p id="tampilkan"></p>
                <p>Lokasi anda! </p>
                 
                <div id="mapcanvas"></div>
                </center>
                <script src="http://maps.google.com/maps/api/js"></script>
                 
                <script>
                var view = document.getElementById("tampilkan");
               
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(showPosition, showError);
                    } else {
                        view.innerHTML = "Yah browsernya ngga support Geolocation bro!";
                    }
              
                 
                function showPosition(position) {
                    lat = position.coords.latitude;
                    lon = position.coords.longitude;
                    latlon = new google.maps.LatLng(lat, lon)
                    mapcanvas = document.getElementById('mapcanvas')
                    mapcanvas.style.height = '500px';
                    mapcanvas.style.width = '500px';
                 
                    var myOptions = {
                    center:latlon,
                    zoom:14,
                    mapTypeId:google.maps.MapTypeId.ROADMAP
                    }
                     
                    var map = new google.maps.Map(document.getElementById("mapcanvas"), myOptions);
                    var marker = new google.maps.Marker({
                        position:latlon,
                        map:map,
                        title:"You are here!"
                    });
                }
                 
                function showError(error) {
                    switch(error.code) {
                        case error.PERMISSION_DENIED:
                            view.innerHTML = "Yah, mau deteksi lokasi tapi ga boleh :("
                            break;
                        case error.POSITION_UNAVAILABLE:
                            view.innerHTML = "Yah, Info lokasimu nggak bisa ditemukan nih"
                            break;
                        case error.TIMEOUT:
                            view.innerHTML = "Requestnya timeout bro"
                            break;
                        case error.UNKNOWN_ERROR:
                            view.innerHTML = "An unknown error occurred."
                            break;
                    }
                 }
                </script>
            </div>
        
          </div>
        </section>
      </aside><!-- /.control-sidebar -->
    </div>
    <!-- /#wrapper -->
</body>

    <?php $this->load->view('template/_bottom'); ?>
</html>