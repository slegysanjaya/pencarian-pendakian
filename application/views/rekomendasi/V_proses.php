<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/header_table'); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <?php $this->load->view('template/menu'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Hasil Analisa
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Hasil Analisa</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Data Alternatif dan Kriteria</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Ketinggian  (MDPL)</center>
                        <th><center>Wilayah</center>
                        <th><center>Transportasi</center>
                        <th><center>Jarak (KM)</center>
                        <th><center>Jarak PP (KM)</center>
                        <th><center>Biaya Makan</center>
                        <th><center>Biaya Masuk</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($i=0; $i<count($arr); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $arr[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo $arr[$i]['ketinggian']; ?></center></td>
                            <td><center><?php echo $arr[$i]['wilayah']; ?></center></td>
                            <td><center><?php echo $arr[$i]['transportasi']; ?></center></td>
                            <td><center><?php echo $arr[$i]['jarak']/2; ?></center></td>
                            <td><center><?php echo $arr[$i]['jarak']; ?></center></td>
                            <td><center><?php echo $arr[$i]['harga'].'/hari'; ?></center></td>
                            <td><center><?php echo $arr[$i]['biaya_masuk']; ?></center></td>
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->




           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Konversi Data Wisata</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Jarak</center>
                        <th><center>Ketinggian</center>
                        <!-- <th><center>Transportasi</center> -->
                        <th><center>Biaya</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; 
                            $total_jarak = 0;
                            $total_ketinggian = 0;
                            $total_transportasi = 0;
                            $total_biaya = 0;

                          ?>

                        <?php for ($i=0; $i<count($data_bobot); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $data_bobot[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo $data_bobot[$i]['jarak']; $total_jarak = $total_jarak + ($data_bobot[$i]['jarak']*$data_bobot[$i]['jarak']); ?></center></td>
                            <td><center><?php echo $data_bobot[$i]['ketinggian']; $total_ketinggian = $total_ketinggian + ($data_bobot[$i]['ketinggian']*$data_bobot[$i]['ketinggian']); ?></center></td>
                            <!-- <td><center> --><?php $data_bobot[$i]['transportasi']; $total_transportasi = $total_transportasi + ($data_bobot[$i]['transportasi']*$data_bobot[$i]['transportasi']); ?><!-- </center></td> -->
                            <td><center><?php echo $data_bobot[$i]['biaya']; $total_biaya = $total_biaya + ($data_bobot[$i]['biaya']*$data_bobot[$i]['biaya']); ?></center></td>
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Jumlah Hasil Akar Pangkat</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th><center>Jarak</center>
                        <th><center>Ketinggian</center>
                        <!-- <th><center>Transportasi</center> -->
                        <th><center>Biaya</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; 
                          $sqrt_jarak = 0;
                          $sqrt_ketinggian = 0;
                          $sqrt_transportasi = 0;
                          $sqrt_biaya = 0;

                        ?>
                       
                        <tr>
                            <td><center><?php $sqrt_jarak = round(sqrt($total_jarak),4); echo $sqrt_jarak; ?></center></td>
                            <td><center><?php $sqrt_ketinggian = round(sqrt($total_ketinggian),4); echo $sqrt_ketinggian; ?></center></td>
                            <!-- <td><center> --><?php $sqrt_transportasi = round(sqrt($total_transportasi),4); //$sqrt_transportasi; ?><!-- </center></td> -->
                            <td><center><?php $sqrt_biaya = round(sqrt($total_biaya),4); echo $sqrt_biaya; ?></center></td>
                        </tr>
      
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          <?php
          $n = 0;
          for($i=0; $i<count($data_bobot); $i++){
            $arr_normalisasi[$n]['id'] = $data_bobot[$i]['id'];
            $arr_normalisasi[$n]['alternatif'] = $data_bobot[$i]['alternatif'];
            $arr_normalisasi[$n]['jarak'] = round(($data_bobot[$i]['jarak']/$sqrt_jarak),4);
            $arr_normalisasi[$n]['ketinggian'] = round(($data_bobot[$i]['ketinggian']/$sqrt_ketinggian),4);
            $arr_normalisasi[$n]['transportasi'] = round(($data_bobot[$i]['transportasi']/$sqrt_transportasi),4);
            $arr_normalisasi[$n]['biaya'] = round(($data_bobot[$i]['biaya']/$sqrt_biaya),4);
            $n++;
          }

           ?>



           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Matrik Ternormalisasi</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                       <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Jarak</center>
                        <th><center>Ketinggian</center>
                        <!-- <th><center>Transportasi</center> -->
                        <th><center>Biaya</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($i=0; $i<count($arr_normalisasi); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $arr_normalisasi[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo $arr_normalisasi[$i]['jarak']; ?></center></td>
                            <td><center><?php echo $arr_normalisasi[$i]['ketinggian']; ?></center></td>
                            <!-- <td><center> <?php //echo $arr_normalisasi[$i]['transportasi']; ?> </center></td> -->
                            <td><center><?php echo $arr_normalisasi[$i]['biaya']; ?></center></td>
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->



          <?php
          $n = 0;
          for($i=0; $i<count($arr_normalisasi); $i++){
            $arr_normalisasi_bobot[$n]['id'] =  $arr_normalisasi[$i]['id'];
            $arr_normalisasi_bobot[$n]['alternatif'] = $arr_normalisasi[$i]['alternatif'];
            $arr_normalisasi_bobot[$n]['jarak'] = $arr_normalisasi[$i]['jarak']*5;
            $arr_normalisasi_bobot[$n]['ketinggian'] = $arr_normalisasi[$i]['ketinggian']*3;
            $arr_normalisasi_bobot[$n]['transportasi'] = $arr_normalisasi[$i]['transportasi']*1;
            $arr_normalisasi_bobot[$n]['biaya'] = $arr_normalisasi[$i]['biaya']*5;
            $n++;
          }

           ?>


           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Matrik Ternormalisasi Terbobot</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                       <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Jarak</center>
                        <th><center>Ketinggian</center>
                       <!--  <th><center>Transportasi</center> -->
                        <th><center>Biaya</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($i=0; $i<count($arr_normalisasi_bobot); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $arr_normalisasi_bobot[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo $arr_normalisasi_bobot[$i]['jarak']; ?></center></td>
                            <td><center><?php echo $arr_normalisasi_bobot[$i]['ketinggian']; ?></center></td>
                            <!-- <td><center><?php //echo $arr_normalisasi_bobot[$i]['transportasi']; ?></center></td> -->
                            <td><center><?php echo $arr_normalisasi_bobot[$i]['biaya']; ?></center></td>
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <?php
          $n = 0;
          for($i=0; $i<count($arr_normalisasi_bobot); $i++){

            $jarak[$n] = $arr_normalisasi_bobot[$i]['jarak'];
            $ketinggian[$n] = $arr_normalisasi_bobot[$i]['ketinggian'];
            $transportasi[$n] = $arr_normalisasi_bobot[$i]['transportasi'];
            $biaya[$n] = $arr_normalisasi_bobot[$i]['biaya'];
            $n++;
          }

          $max_jarak = max($jarak);
          $max_ketinggian = max($ketinggian);
          $max_transportasi = max($transportasi);
          $max_biaya = max($biaya);

          $min_jarak = min($jarak);
          $min_ketinggian = min($ketinggian);
          $min_transportasi = min($transportasi);
          $min_biaya = min($biaya);



           ?>

           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Solusi Ideal Positif (+) dan Solusi Ideal Negatif (-)</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th><center>Solusi</center>
                        <th><center>Jarak</center>
                        <th><center>Ketinggian</center>
                        <!-- <th><center>Transportasi</center> -->
                        <th><center>Biaya</center>
                        
                    </thead>
                    <tbody>
                        
                       
                        <tr>
                            <td><center>(+)</center></td>
                            <td><center><?php echo $max_jarak; ?></center></td>
                            <td><center><?php echo $max_ketinggian; ?></center></td>
                            <!-- <td><center><?php //echo $max_transportasi; ?></center></td> -->
                            <td><center><?php echo $max_biaya; ?></center></td>
                        </tr>

                        <tr>
                            <td><center>(-)</center></td>
                            <td><center><?php echo $min_jarak; ?></center></td>
                            <td><center><?php echo $min_ketinggian; ?></center></td>
                            <!-- <td><center><?php //echo $min_transportasi; ?></center></td> -->
                            <td><center><?php echo $min_biaya; ?></center></td>
                        </tr>
      
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <?php
          $n = 0;
          for($i=0; $i<count($arr_normalisasi_bobot); $i++){
            $solusi[$n]['id'] = $arr_normalisasi_bobot[$i]['id'];
            $solusi[$n]['alternatif'] = $arr_normalisasi_bobot[$i]['alternatif'];
            $solusi[$n]['positif'] = sqrt((($arr_normalisasi_bobot[$i]['jarak']-$max_jarak)*($arr_normalisasi_bobot[$i]['jarak']-$max_jarak))+(($arr_normalisasi_bobot[$i]['ketinggian']-$max_ketinggian)*($arr_normalisasi_bobot[$i]['ketinggian']-$max_ketinggian))+(($arr_normalisasi_bobot[$i]['transportasi']-$max_transportasi)*($arr_normalisasi_bobot[$i]['transportasi']-$max_transportasi))+(($arr_normalisasi_bobot[$i]['biaya']-$max_biaya)*($arr_normalisasi_bobot[$i]['biaya']-$max_biaya)));
            $solusi[$n]['negatif'] = sqrt((($arr_normalisasi_bobot[$i]['jarak']-$min_jarak)*($arr_normalisasi_bobot[$i]['jarak']-$min_jarak))+(($arr_normalisasi_bobot[$i]['ketinggian']-$min_ketinggian)*($arr_normalisasi_bobot[$i]['ketinggian']-$min_ketinggian))+(($arr_normalisasi_bobot[$i]['transportasi']-$min_transportasi)*($arr_normalisasi_bobot[$i]['transportasi']-$min_transportasi))+(($arr_normalisasi_bobot[$i]['biaya']-$min_biaya)*($arr_normalisasi_bobot[$i]['biaya']-$min_biaya)));
            $n++;
          }


           ?>


           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Jarak Ideal Solusi Positif & Negatif</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                       <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Positif</center>
                        <th><center>Negatif</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($i=0; $i<count($solusi); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $solusi[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo round($solusi[$i]['positif'],4); ?></center></td>
                            <td><center><?php echo round($solusi[$i]['negatif'],4); ?></center></td>
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          <?php
            $n = 0;

            for($i=0; $i<count($solusi); $i++){
                $hasil[$n]['id'] = $solusi[$i]['id'];
                $hasil[$n]['alternatif'] = $solusi[$i]['alternatif'];
                $hasil[$n]['bobot'] = round(1-($solusi[$i]['negatif']/($solusi[$i]['negatif']+$solusi[$i]['positif'])),4);
                $n++;
            }


           ?>


           <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Nilai Ideal Setiap Alternatif</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                       <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Bobot</center>
                       
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($i=0; $i<count($hasil); $i++): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $hasil[$i]['alternatif']; ?></center></td>
                            <td><center><?php echo $hasil[$i]['bobot']; ?></center></td>
                      
                        </tr>
                        <?php endfor ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->

          <?php 
            $tmp=array();
            foreach ($hasil as $ma) {
                $tmp[]= &$ma['bobot'];
            }
            array_multisort($tmp, SORT_DESC, $hasil);

          ?>


         <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Urutan Rekomendasi</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Ketinggian  (MDPL)</center>
                        <th><center>Wilayah</center>
                        <th><center>Transportasi</center>
                        <th><center>Jarak (KM)</center>
                        <th><center>Jarak PP (KM)</center>
                        <th><center>Biaya Makan</center>
                        <th><center>Biaya Masuk</center>
                        <th><center>Total Biaya (Ribu)</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php for ($j=0; $j<count($hasil); $j++): ?>
                          <?php for ($i=0; $i<count($arr); $i++): ?>
                            <?php if($hasil[$j]['id'] == $arr[$i]['id']) : ?>

                              <tr <?php if($j<3) echo 'style="background-color: #EEC914;"' ?> >
                                  <td><center><?php echo $no; $no++; ?></center></td>
                                  <td><center><?php echo $arr[$i]['alternatif']; ?></center></td>
                                  <td><center><?php echo $arr[$i]['ketinggian']; ?></center></td>
                                  <td><center><?php echo $arr[$i]['wilayah']; ?></center></td>
                                  <td><center><?php echo $arr[$i]['transportasi']; ?></center></td>
                                  <td><center><?php echo $arr[$i]['jarak']/2; ?></center></td>
                                  <td><center><?php echo $arr[$i]['jarak']; ?></center></td>
                                  <td><center><?php echo "Rp. " . number_format($arr[$i]['harga'], 0, ',', '.').'/hari '; ?></center></td>
                                  <td><center><?php echo "Rp. " . number_format($arr[$i]['biaya_masuk'], 0, ',', '.'); ?></center></td>
                                  <td><center><?php echo "Rp. " . number_format($arr[$i]['total_biaya'], 0, ',', '.') ; ?></center></td>
                              </tr>
                            <?php endif ?>
                          <?php endfor ?>
                        <?php endfor ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->


          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                <center><h3>Tabel Urutan Rekomendasi Biaya yang diinginkan</h3></center>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Alternatif</center>
                        <th><center>Ketinggian  (MDPL)</center>
                        <th><center>Wilayah</center>
                        <th><center>Transportasi</center>
                        <th><center>Jarak (KM)</center>
                        <th><center>Jarak PP (KM)</center>
                        <th><center>Biaya Makan</center>
                        <th><center>Biaya Masuk</center>
                        <th><center>Total Biaya (Ribu)</center>
                        
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>

                        <?php for ($j=0; $j<count($hasil); $j++): ?>
                            <?php for ($i=0; $i<count($arr); $i++): ?>
                              <?php if($hasil[$j]['id'] == $arr[$i]['id']) : ?>
                            
                                <?php if ($arr[$i]['bobot_biaya'] <= $biaya_user): ?>

                                  <tr <?php if($no <= 3) echo 'style="background-color: #EEC914;"' ?> >
                                      <td><center><?php echo $no; $no++; ?></center></td>
                                      <td><center><?php echo $arr[$i]['alternatif']; ?></center></td>
                                      <td><center><?php echo $arr[$i]['ketinggian']; ?></center></td>
                                      <td><center><?php echo $arr[$i]['wilayah']; ?></center></td>
                                      <td><center><?php echo $arr[$i]['transportasi']; ?></center></td>
                                      <td><center><?php echo $arr[$i]['jarak']/2; ?></center></td>
                                      <td><center><?php echo $arr[$i]['jarak']; ?></center></td>
                                      <td><center><?php echo "Rp. " . number_format($arr[$i]['harga'], 0, ',', '.').'/Box '; ?>(Lihat)</a></center></td>
                                      <td><center><?php echo "Rp. " . number_format($arr[$i]['biaya_masuk'], 0, ',', '.'); ?></center></td>
                                      <td><center><?php echo "Rp. " . number_format($arr[$i]['total_biaya'], 0, ',', '.') ; ?></center></td>
                                  </tr>
                                <?php endif ?>

                              <?php endif ?>
                            <?php endfor ?>
                        <?php endfor ?>
                        <?php if ($no == 1): ?>
                            <tr>
                              <td colspan="9"><center>Tidak ada data yang sesuai dengan keinginan anda</center></td>
                            </tr>
                        <?php endif ?>
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->




        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      





      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


    <?php $this->load->view('template/bottom_table'); ?>
    

  </body>
</html>
