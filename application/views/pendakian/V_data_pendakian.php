<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/header_table'); ?>
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <?php $this->load->view('template/menu'); ?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            Data Pendakian
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Data Pendakian</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-xs-12">
              <div class="box">
                <div class="box-header">
                 
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <th class="col-lg-1"><center>No</center></th>
                        <th><center>Nama Wisata</center>
                        <th><center>Ketinggian  (MDPL)</center>
                        <th><center>Wilayah</center>
                        <th><center>Biaya Masuk</center>
                        <th><center>Provinsi</center>
                        <th><center>Makan Per Hari</center>
                        <th class="col-md-1"><center><a href="<?php echo base_url(); ?>Pendakian/tambah" class="btn btn-primary btn-small" data-toggle="modal" ><i class="fa fa-plus"> Tambah Pendakian</i></a></center></th>
                    </thead>
                    <tbody>
                        <?php $no = 1; ?>
                        <?php foreach ($data_pendakian->result() as $key): ?>

                        <tr>
                            <td><center><?php echo $no; $no++; ?></center></td>
                            <td><center><?php echo $key->nama_wisata; ?></center></td>
                            <td><center><?php echo $key->ketinggian; ?></center></td>
                            <td><center><?php echo $key->wilayah; ?></center></td>
                            <td><center><?php echo $key->biaya_masuk; ?></center></td>
                            <td><center><?php echo $key->provinsi; ?></center></td>
                            <td><center><?php echo $key->id_paket; ?></center></td>
                            <td ><center>
                                <div class="tooltip-demo">
                                    <a class="btn btn-warning" data-toggle="modal" data-target="#modal_edit_<?php echo $key->id; ?>" data-placement="top" title="Ubah" ><i class="fa fa-Edit"> Ubah</i></a>
                                    <a class="btn btn-danger" data-toggle="modal" data-target="#modal_hapus_<?php echo $key->id; ?>" data-placement="top" title="Hapus" ><i class="fa fa-trash-o"> Hapus</i></a>
                                </div></center>
                            </td>
                        </tr>
                        <?php endforeach ?>
                        
                    </tbody>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div><!-- /.col -->
          </div><!-- /.row -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->


      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->


<!-- Start modal edit Modul -->
<?php foreach ($data_pendakian->result() as $key): ?>
<div class="modal fade" id="modal_edit_<?php echo $key->id; ?>">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><center>Ubah Data Pendakian</center></h4>
            </div>
                <div class="modal-body" style="margin-bottom:-30px;">
                <form method="post" action="<?php echo base_url(); ?>Pendakian/edit">
                    <table class="table table-hover">
                        <tr>
                            <td>Nama Wisata</td>
                            <td>:</td>
                            <td><input name="nama_wisata" type="text" class="form-control" placeholder="" value="<?php echo $key->nama_wisata; ?>"></td>
                        </tr>
                        <tr>
                            <td>Ketinggian</td>
                            <td>:</td>
                            <td><input name="ketinggian" type="text" class="form-control" placeholder="" value="<?php echo $key->ketinggian; ?>"></td>
                        </tr>
                        <tr>
                            <td>Wilayah</td>
                            <td>:</td>
                            <td><input name="wilayah" type="text" class="form-control" placeholder="" value="<?php echo $key->wilayah; ?>"></td>
                        </tr>
                        <tr>
                            <td>Provinsis</td>
                            <td>:</td>
                            <td><input name="provinsi" type="text" class="form-control" placeholder="" value="<?php echo $key->provinsi; ?>"></td>
                        </tr>
                        <tr>
                            <td>Biaya Masuk</td>
                            <td>:</td>
                            <td><input name="biaya_masuk" type="text" class="form-control" placeholder="" value="<?php echo $key->biaya_masuk; ?>"></td>
                        </tr>
                       <!--  <tr>
                            <td>Paket</td>
                            <td>:</td>
                            <td>
                                <select name="id_paket" class="form-control">
                                    <option value="">Pilih paket</option>
                                    <?php //foreach($paket->result() as $row) :?>
                                        <option value="<?php //echo $row->id ?>" <?php //if($key->id_paket == $row->id) echo "selected" ?>> <?php //echo $row->nama_paket.' - '.$row->harga; ?></option>
                                    <?php //endforeach ?>
                                </select> 
                            </td>
                        </tr> -->
                        <tr><td></td></tr>
                        <input name="id" type="hidden" value="<?php echo $key->id ?>">                        
                        
                    </table>
                </div> <!-- end modal-body -->
                <div class="modal-footer" style="margin-bottom:-20px;">
                    <input name="simpan" type="submit" class="btn btn-primary" value="Simpan">
                    <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div> <!-- modal content -->
    </div> <!-- modal dialog -->
</div>
<?php endforeach ?>
<!-- End modal EDIT Modul -->


<!-- Start Modal Hapus -->
<?php foreach ($data_pendakian->result() as $key): ?>
<div class="modal fade" id="modal_hapus_<?php echo $key->id; ?>">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Konfirmasi</h4>
            </div>
            <div class="modal-body" style="margin-bottom:-20px;">
                <div class="alert alert-danger">Anda yakin akan menghapus data <strong><?php echo $key->nama_wisata; ?></strong> ?</div>
            </div>
            <div class="modal-footer">
                <form method="post" action="<?php echo base_url(); ?>Pendakian/hapus/<?php echo $key->id; ?>">
                    <input name="id" type="hidden" value="<?php echo $key->id; ?>" >
                    <button type="submit" class="btn btn-default btn-primary" href="">Ya</button>
                    <button type="button" class="btn btn-default btn-default" data-dismiss="modal">Batal</button>
                </form>
            </div>
        </div>
    </div>
</div>
<?php endforeach ?>
<!-- End Start Modal Hapus -->


    <?php $this->load->view('template/bottom_table'); ?>
    

  </body>
</html>
