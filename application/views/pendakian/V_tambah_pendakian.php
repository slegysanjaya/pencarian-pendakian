<!DOCTYPE html>
<html>
  <head>
    <?php $this->load->view('template/_header'); ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">


    <div id="wrapper">
        <!-- Content Wrapper. Contains page content -->
    <?php $this->load->view('template/menu'); ?>

      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
         
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Tambah Pendakian</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Tambah Pendakian</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" method="post" action="<?php echo base_url(); ?>Pendakian/tambah">
                  <div class="box-body">
                    <div class="form-group">
                      <label for="exampleInputEmail1">Nama Wisata</label>
                      <input type="text" name="nama_wisata" class="form-control" id="exampleInputEmail1" placeholder="nama wisata">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Ketinggian</label>
                      <input type="text" name="ketinggian" class="form-control" id="exampleInputEmail1" placeholder="ketinggian">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Wilayah</label>
                      <input type="text" name="wilayah" class="form-control" id="exampleInputEmail1" placeholder="wilayah">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Biaya Masuk</label>
                      <input type="text" name="biaya_masuk" class="form-control" id="exampleInputEmail1" placeholder="biaya masuk">
                    </div>
                    <div class="form-group">
                      <label for="exampleInputEmail1">Provinsi</label>
                      <input type="text" name="provinsi" class="form-control" id="exampleInputEmail1" placeholder="provinsi">
                    </div>
                    <!-- <div class="form-group">
                      <label for="exampleInputEmail1">Makan per Hari</label>
                        <input type="text" name="id_paket"  class="form-control" id="exampleInputEmail1" placeholder="provinsi">
                      <select name="id_paket" class="form-control" id="exampleInputEmail1">
                            <option value="">Pilih paket</option>
                        <?php //foreach($paket->result() as $row) :?>
                            <option value="<?php //echo $row->id ?>"> <?php //echo $row->nama_paket.' - '.$row->harga; ?></option>
                        <?php //endforeach ?>
                      </select> 
                    </div> -->
                  </div><!-- /.box-body -->

                  <div class="box-footer">
                    <button type="submit" value="simpan" name="simpan" class="btn btn-primary">Submit</button>
                  </div>
                </form>
              </div><!-- /.box -->
            </div>
          </div>
        </section>
      </aside><!-- /.control-sidebar -->
    </div>
    <!-- /#wrapper -->
</body>

    <?php $this->load->view('template/_bottom'); ?>
</html>