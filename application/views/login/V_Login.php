<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Login</title>

    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>_assets/sb-admin-2/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="<?php echo base_url(); ?>_assets/sb-admin-2/css/plugins/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>_assets/sb-admin-2/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="<?php echo base_url(); ?>_assets/sb-admin-2/font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">

     <style type="text/css">
    html {height:100%;}
    body {
        background-image: url('<?php echo base_url(); ?>_assets/default/images/DSC_0191.jpg');
        -moz-background-size: cover;
        -webkit-background-size: cover;
        background-size: cover;
        background-position: top center !important;
        background-repeat: no-repeat !important;
        background-attachment: fixed;
    }
    </style>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body><br>
<center><strong>
    <h2>SISTEM INFORMASI PENDUKUNG KEPUTUSAN MENENTUKAN WISATA PENDAKIAN</h2>
</strong></center>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form method="post" action="<?php echo base_url(); ?>login/cek_login">
                        <table class="table table-hover">
                        <tr>
                            <td class="col-lg-3">Username</td>
                            <td>:</td>
                            <td><input name="username" type="text" class="form-control" placeholder="Username" value="" required ></td>
                        </tr>
                        <tr>
                            <td class="col-lg-3">Password</td>
                            <td>:</td>
                            <td><input name="password" type="password" class="form-control" placeholder="Password" value="" required ></td>
                        </tr>
                        </table>
                        <div class="modal-footer">
                            <button id="simpan" name="simpan" type="submit" class="btn btn-primary">Login</button>
                            <a href="<?php echo base_url(); ?>login/daftar" type="button" class="btn btn-default btn-default">Daftar</a>
                        </div>
                    </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery Version 1.11.0 -->
    <script src="<?php echo base_url(); ?>_assets/sb-admin-2/js/jquery-1.11.0.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>_assets/sb-admin-2/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>_assets/sb-admin-2/js/plugins/metisMenu/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>_assets/sb-admin-2/js/sb-admin-2.js"></script>

</body>

</html>
