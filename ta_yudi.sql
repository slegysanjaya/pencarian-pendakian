-- phpMyAdmin SQL Dump
-- version 4.6.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 18, 2016 at 01:09 AM
-- Server version: 5.7.13-log
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ta_yudi`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_paket`
--

CREATE TABLE `detail_paket` (
  `id` int(11) NOT NULL,
  `id_paket` int(11) NOT NULL,
  `keterangan` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `detail_paket`
--

INSERT INTO `detail_paket` (`id`, `id_paket`, `keterangan`) VALUES
(1, 1, 'Nasi'),
(2, 1, 'Ayam'),
(3, 1, 'Tahu'),
(4, 1, 'Tempe');

-- --------------------------------------------------------

--
-- Table structure for table `tb_info_gunung`
--

CREATE TABLE `tb_info_gunung` (
  `id` int(5) NOT NULL,
  `nama_gunung` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_info_gunung`
--

INSERT INTO `tb_info_gunung` (`id`, `nama_gunung`, `keterangan`, `foto`) VALUES
(1, 'Gunung Bromo', 'Gunung Bromo adalah salah satu gunung berapi aktif yang ada di Indonesia, tepatnya di Jawa Timur dan meliputi 4 kabupaten yaitu Kabupaten Probolinggo, Pasuruan, Lumajang, dan Kabupaten Malang. Sebagai gunung berapi yang masih aktif, Bromo jadi tujuan wisata terkenal di Jawa Timur dan hampir tidak pernah sepi setiap harinya.\r\n\r\nStatusnya yang masih aktif membuat Gunung Bromo jadi lebih menarik di mata wisatawan. Ketinggian Gunung Bromo 2.392 meter diatas permukaan laut dan memiliki bentuk tubuh bertautan diantara lembah dan ngarai dengan di kelilingi kaldera atau lautan pasir luas kurang lebih sekitar 5.300 hektar.\r\n\r\nGunung Bromo terkenal sebagai icon wisata probolinggo paling indah dan paling banyak dikunjungi. Kata “Bromo” berasal dari kata “Brahma”  yaitu salah satu Dewa Agama Hindu. Gunung Bromo memang tidak besar seperti gunung api lain di Indonesia tetapi pemandangan Bromo sangat menakjubkan sekali. Keindahan Gunung Bromo yang luar biasa membuat wisatawan kagum.\r\n\r\nDari puncak Penanjakan pada ketinggian 2.780 m, wisatawan bisa melihat matahari terbit di Wisata Bromo. Pemandangan yang indah membuat banyak wisatawan ingin mengabadikan momen berharga ini. Pada waktu matahari terbit terlihat dari puncak penanjakan yang sangat luar biasa para pengunjung bisa melihat latar depan dari Gunung Semeru yang akan mengeluarkan asap terlihat dari kejauhan dan matahari akan bersinar terang naik ke atas langit.', 'Sejarah-Dari-Gunung-Bromo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_paket`
--

CREATE TABLE `tb_paket` (
  `id` int(11) NOT NULL,
  `nama_paket` varchar(50) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_paket`
--

INSERT INTO `tb_paket` (`id`, `nama_paket`, `harga`) VALUES
(1, 'Paket 1', 21000),
(2, 'Paket 2', 30000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_pendakian`
--

CREATE TABLE `tb_pendakian` (
  `id` int(11) NOT NULL,
  `nama_wisata` varchar(50) NOT NULL,
  `ketinggian` varchar(50) NOT NULL,
  `wilayah` varchar(50) NOT NULL,
  `biaya_masuk` int(11) NOT NULL,
  `provinsi` varchar(50) NOT NULL,
  `id_paket` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_pendakian`
--

INSERT INTO `tb_pendakian` (`id`, `nama_wisata`, `ketinggian`, `wilayah`, `biaya_masuk`, `provinsi`, `id_paket`) VALUES
(1, 'Gunung Selamet', '3428', 'Kab. Banyumas', 10000, 'Jawa Tengah', 45000),
(2, 'Gunung Semeru', '3676', 'Kab. Lumajang', 75000, 'Jawa Timur', 45000),
(3, 'Gunung Raung', '3332', 'Kab. Bondowoso', 10000, 'Jawa Timur', 45000),
(4, 'Gunung Kelud', '1731', 'Kab. Kediri', 10000, 'Jawa Timur', 45000),
(5, 'Gunun Ijen', '2443', 'Kab. Banyuwangi', 32500, 'Jawa Timur', 45000),
(6, 'Gunung Manglayang', '1818', 'Kab. Bandung', 5000, 'Jawa Barat', 45000),
(7, 'Gunung Batu', '875', 'Bogor', 10000, 'Jawa Barat', 45000),
(8, 'Gunung Ciremai', '3078', 'Majalengka', 50000, 'Jawa Barat', 45000),
(9, 'Gunung Andong', '1463', 'Kab. Magelang', 5000, 'Jawa Tengah', 45000),
(10, 'Gunung Merapi', '2911', 'Kab. Klaten', 10000, 'Jawa Tengah', 45000);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `level` enum('administrator','pengguna') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'administrator'),
(2, 'cek', 'cek', 'pengguna'),
(3, 'bro', 'bro', 'pengguna');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_paket`
--
ALTER TABLE `detail_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_info_gunung`
--
ALTER TABLE `tb_info_gunung`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_paket`
--
ALTER TABLE `tb_paket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_pendakian`
--
ALTER TABLE `tb_pendakian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `detail_paket`
--
ALTER TABLE `detail_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tb_info_gunung`
--
ALTER TABLE `tb_info_gunung`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tb_paket`
--
ALTER TABLE `tb_paket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tb_pendakian`
--
ALTER TABLE `tb_pendakian`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
